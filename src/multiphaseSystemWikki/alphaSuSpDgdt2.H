volScalarField::Internal Sp
(
    IOobject
    (
        "Sp",
        mesh_.time().timeName(),
        mesh_
    ),
    mesh_,
    dimensionedScalar("zero", dimless/dimTime, 0)
);

volScalarField::Internal Su
(
    IOobject
    (
        "Su",
        mesh_.time().timeName(),
        mesh_
    ),
    mesh_,
    dimensionedScalar("zero", dimless/dimTime, 0)
);

if (phase.divU().valid())
{
    const scalarField& dgdt = phase.divU()();
    forAll(dgdt, celli)
    {
        if (dgdt[celli] > 0.0)
        {
            Sp[celli] -= dgdt[celli];
            Su[celli] += dgdt[celli];
        }
        else if (dgdt[celli] < 0.0)
        {
            Sp[celli] +=
                dgdt[celli]
               *(1 - alpha[celli])/max(alpha[celli], 1e-4);
        }
    }
}

// volScalarField::Internal divU2 (fvc::div(phase.divU()));
// volScalarField::Internal divU2 = phase.divU()();
volScalarField::Internal divU2 (fvc::div(phi_));
